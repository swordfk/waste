package com.example.myapplication2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Step1 extends AppCompatActivity {
    ImageView imgL1, imgL2, imgL3, imgL4, imgL5, imgL6;
    LinearLayout l1,l2,l3,l4,l5,l6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step1);

        Button next = findViewById(R.id.btnNext);
        Button back = findViewById(R.id.btnBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Step2.class));
            }
        });

        l1 = findViewById(R.id.layout1);
        imgL1 = findViewById(R.id.imgL1);
        l2 = findViewById(R.id.layout2);
        imgL2 = findViewById(R.id.imgL2);
        l3 = findViewById(R.id.layout3);
        imgL3 = findViewById(R.id.imgL3);
        l4 = findViewById(R.id.layout4);
        imgL4 = findViewById(R.id.imgL4);
        l5 = findViewById(R.id.layout5);
        imgL5 = findViewById(R.id.imgL5);
        l6 = findViewById(R.id.layout6);
        imgL6 = findViewById(R.id.imgL6);

        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check(1);
            }
        });
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check(2);
            }
        });
        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check(3);
            }
        });
        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check(4);
            }
        });
        l5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check(5);
            }
        });
        l6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check(6);
            }
        });

    }

    private void check(int id){
        switch (id){
            case 1:
                imgL1.setImageResource(R.drawable.check);
                imgL2.setImageResource(R.drawable.elips);
                imgL3.setImageResource(R.drawable.elips);
                imgL4.setImageResource(R.drawable.elips);
                imgL5.setImageResource(R.drawable.elips);
                imgL6.setImageResource(R.drawable.elips);
                break;
            case 2:
                imgL2.setImageResource(R.drawable.check);
                imgL1.setImageResource(R.drawable.elips);
                imgL3.setImageResource(R.drawable.elips);
                imgL4.setImageResource(R.drawable.elips);
                imgL5.setImageResource(R.drawable.elips);
                imgL6.setImageResource(R.drawable.elips);
                break;
            case 3:
                imgL3.setImageResource(R.drawable.check);
                imgL2.setImageResource(R.drawable.elips);
                imgL1.setImageResource(R.drawable.elips);
                imgL4.setImageResource(R.drawable.elips);
                imgL5.setImageResource(R.drawable.elips);
                imgL6.setImageResource(R.drawable.elips);
                break;
            case 4:
                imgL4.setImageResource(R.drawable.check);
                imgL1.setImageResource(R.drawable.elips);
                imgL2.setImageResource(R.drawable.elips);
                imgL3.setImageResource(R.drawable.elips);
                imgL5.setImageResource(R.drawable.elips);
                imgL6.setImageResource(R.drawable.elips);
                break;
            case 5:
                imgL5.setImageResource(R.drawable.check);
                imgL2.setImageResource(R.drawable.elips);
                imgL1.setImageResource(R.drawable.elips);
                imgL3.setImageResource(R.drawable.elips);
                imgL4.setImageResource(R.drawable.elips);
                imgL6.setImageResource(R.drawable.elips);
                break;
            case 6:
                imgL6.setImageResource(R.drawable.check);
                imgL1.setImageResource(R.drawable.elips);
                imgL2.setImageResource(R.drawable.elips);
                imgL3.setImageResource(R.drawable.elips);
                imgL4.setImageResource(R.drawable.elips);
                imgL5.setImageResource(R.drawable.elips);
                break;

        }
    }
}