package com.example.myapplication2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class DaftarFragment extends Fragment {
    Button daftar;
    EditText edHp, edPass, edNama, edKelamin, edKonfirmasiPass, edTgl, edEmail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_daftar, container, false);

        daftar = v.findViewById(R.id.btnDaftar2);
        edHp = v.findViewById(R.id.edHp);
        edPass = v.findViewById(R.id.edPass);
        edKonfirmasiPass = v.findViewById(R.id.edKonfirmasi);
        edEmail = v.findViewById(R.id.edEmail);
        edNama = v.findViewById(R.id.edNama);
        edKelamin = v.findViewById(R.id.edKelamin);
        edTgl = v.findViewById(R.id.edTgl);

        daftar.setEnabled(false);

        edHp.addTextChangedListener(textWatcher);
        edPass.addTextChangedListener(textWatcher);
        edKonfirmasiPass.addTextChangedListener(textWatcher);
        edEmail.addTextChangedListener(textWatcher);
        edNama.addTextChangedListener(textWatcher);
        edKelamin.addTextChangedListener(textWatcher);
        edTgl.addTextChangedListener(textWatcher);

        return v;
    }
    // implement the TextWatcher callback listener
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // get the content of both the edit text
            String emailInput = edEmail.getText().toString();
            String passwordInput = edPass.getText().toString();
            String hp = edHp.getText().toString();
            String konfirmasi = edKonfirmasiPass.getText().toString();
            String tanggal = edTgl.getText().toString();
            String nama = edNama.getText().toString();
            String kelamin = edKelamin.getText().toString();

            // check whether both the fields are empty or not
            daftar.setEnabled(!emailInput.isEmpty() && !passwordInput.isEmpty() && !hp.isEmpty() && !tanggal.isEmpty()
            && !nama.isEmpty() && !konfirmasi.isEmpty() && !kelamin.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}